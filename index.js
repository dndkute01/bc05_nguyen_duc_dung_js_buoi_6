// EX1: Tìm số nguyên dương nhỏ nhất
/*
Input: Tìm số nguyên dương nhỏ nhất! 1+2+..+n >10000

Output: n = 141
*/
function ketQua() {
  var sum = 0;
  var ketQua = "";
  for (var i = 0; i <= 10000; i++) {
    sum += i;
    if (sum > 10000) {
      ketQua = "Số nguyên dương nhỏ nhất: " + i;
      break;
    }
  }
  document.getElementById("ketQua").innerHTML = ketQua;
}

///////////////////////////////////////////////////////////////

// EX2: Tính tổng
/*
Input: nhập vào số x=2 và n=3 vào biểu thức: x + x^2 + x^3 
+ x^n --> Sử dụng loop và function()

Output: ketQua = 14
*/

function ketQua1() {
  var nhapX = document.getElementById("nhapX").value * 1;
  var nhapN = document.getElementById("nhapN").value * 1;

  var bieuThuc = tinhTong(nhapX, nhapN);
  document.getElementById("ketQua1").innerHTML = "Tổng: " + bieuThuc;
}

function tinhTong(x, n) {
  var ketQua = 0;
  for (var i = 1; i <= n; i++) {
    ketQua += Math.pow(x, i);
  }
  return ketQua;
}

//////////////////////////////////////////////////////////////

//EX3: Tính giai thừa

/*
Input: nhập vào n =5 . Tính giai thừa n! = 1* 2 * 3 *...*n

Output: 120
*/

function ketQua2() {
  var soN = document.getElementById("nhapSo").value;
  var giaiThua = 1;
  for (var i = 1; i <= soN; i++) {
    giaiThua *= i;
  }
  document.getElementById("ketQua2").innerHTML = " Giai thừa: " + giaiThua;
}

//////////////////////////////////////////////////////////////

//EX4: Tạo thẻ div

/*
Click button sẽ in ra 10 thẻ div
Nếu div nào vị trí chẵn thì background màu đỏ
lẻ thì màu xanh!
*/

function ketQua3() {
  var output = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 != 0) {
      var div = `<div class="alert alert-primary mb-0 ">Div lẻ</div> `;
      output += div;
    } else {
      var div = `<div class="alert alert-danger mb-0 ">Div chẵn</div>`;
      output += div;
    }
  }
  document.getElementById("theDiv").innerHTML = output;
}
